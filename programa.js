
class CalculadorAritmetico{
    
    //Atributos
    numero1;
    numero2;
    
    //Metodo
    sumar(){
        let r;
        r=this.numero1+this.numero2;
        document.getElementById('resultado').value=r;
    }
    restar(){
        let r;
        r=this.numero1-this.numero2;
        document.getElementById('resultado').value=r;
    }
    modular(){
        let r;
        r=this.numero1%this.numero2;
        document.getElementById('resultado').value=r;
    }
    set sNumero1(n){
        this.numero1=Number(n);
    }
    set sNumero2(n){
        this.numero2=Number(n);
    }
}

class Calculador{
    
    //Atributos
    numero1;
    numero2;

    //Metodos
    potenciar(){
        let r;
        r=this.numero1**this.numero2;
        document.getElementById('resultado').value=r;
    }
    logaritmo(x){
        let r;
        r=Math.log(x);
        document.getElementById('resultado').value=r;
    }
    set sNumero1(n){
        this.numero1=Number(n);
    }
    set sNumero2(n){
        this.numero2=Number(n);
    }

}
let miAritmetico = new CalculadorAritmetico();
let miCalculador=new Calculador();
